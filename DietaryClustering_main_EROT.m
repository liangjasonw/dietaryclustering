% Dietary Temporal Pattern Clustering
% Entropic regularized OT-means only

clc; clear all;
%% Data Readin
data_dir = 'C:\Users\jason\Box Sync\Research\Public health\Collarboration with Yikyung\Data\';
dat = load(fullfile(data_dir,'energy_hourly_1.mat')); % dataset saves in variable "X"
X = dat.x;
dat = load(fullfile(data_dir,'energy_hourly_all.mat')); % dataset saves in variable "Y"
fat_h = load(fullfile(data_dir,'fat_hourly_all.mat')); % dataset saves in variable "x"
carb_h = load(fullfile(data_dir,'carb_hourly_all.mat')); % dataset saves in variable "x"
prot_h = load(fullfile(data_dir,'prot_hourly_all.mat')); % dataset saves in variable "x"
Y_fat = fat_h.x'; Y_carb = carb_h.x'; Y_prot = prot_h.x';
Y = dat.x';
Y_all = Y_fat + Y_carb + Y_prot;
Y_all_ratio = bsxfun(@rdivide, Y_all, sum(Y_all,2)); % calculated from sum of fat, carb and protein, different than Y
[num_sample, ts_len] = size(X);
K_outter = 4; % number of clusters
% shift original data to avoid 0's
delta = 1e-6;
X_tmp = X + delta;
X_shift = bsxfun(@times, X_tmp, 1./(sum(X_tmp, 2)));

%% Entropy regularized OT + K-means
tic;
run_num = 1000; % number of randomized initializations
init_id_EROT = zeros(num_sample,run_num);
member_id_EROT = zeros(num_sample,run_num);
centers_EROT = zeros(K_outter,ts_len,run_num);
obj_EROT = zeros(run_num,1);
parfor i = 1:run_num
    disp(i);
    [init_id, member_id, centers, obj_opt, ~] = kMeans_EROT(X_shift, K_outter);
    init_id_EROT(:,i) = init_id;
    member_id_EROT(:,i) = member_id;
    centers_EROT(:,:,i) = centers;
    obj_EROT(i) = obj_opt;
end
toc;
% post-processing of clustering results
% load('EROT_results_k5_1000.mat');
[sorted_obj_EROT,I] = sort(obj_EROT);
h = figure(1);
alw = 1.5;    % AxesLineWidth
fsz = 64;      % Fontsize
lw = 10;      % LineWidth
msz = 18;       % MarkerSize
plot(sorted_obj_EROT,'LineWidth',lw); grid on;
xlabel('run id','FontSize',fsz);
ylabel('objective value','FontSize',fsz);
set(gca, 'fontsize', fsz, 'LineWidth', alw); %<- Set properties
% print_to_pdf(h,'C:\Users\jason\Desktop\KDD paper\Hybrid results\converg_k4.pdf');
best_init_id_EROT = init_id_EROT(:, I(1));
[init_id, member_id, centers, obj_opt, clusters] = kMeans_EROT(X_shift, K_outter, best_init_id_EROT);
% save('EROT_results_k4_1000.mat','init_id_EROT','member_id_EROT','centers_EROT','obj_EROT');

%% Visualization of some random samples
figure(1);
x_axis = 0:23;
width = 10;     % Width in inches
height = 50;    % Height in inches
alw = 1.5;    % AxesLineWidth
fsz = 32;      % Fontsize
lw = 5;      % LineWidth
msz = 18;       % MarkerSize
pos = get(gcf, 'Position');
% sample_id = find(member_id_new==3);

subplot(5,1,1);
plot(x_axis, X_shift(tmp_idx(sample_id(1)),:),'-*','Color',[0 0.447 0.741],'LineWidth',lw,'MarkerSize',msz);
title('Sample 1','FontSize',fsz);
grid on;
set(gca, 'fontsize', fsz, 'LineWidth', alw, 'YTick',[0 0.2 0.4 0.6 0.8 1]); %<- Set properties
subplot(5,1,2);
plot(x_axis, X_shift(tmp_idx(sample_id(2)),:),'-*','Color',[0 0.447 0.741],'LineWidth',lw,'MarkerSize',msz);
title('Sample 2','FontSize',fsz);
grid on;
set(gca, 'fontsize', fsz, 'LineWidth', alw, 'YTick',[0 0.2 0.4 0.6 0.8 1]); %<- Set properties
subplot(5,1,3);
plot(x_axis, X_shift(tmp_idx(sample_id(3)),:),'-*','Color',[0 0.447 0.741],'LineWidth',lw,'MarkerSize',msz);
title('Sample 3','FontSize',fsz);
grid on;
set(gca, 'fontsize', fsz, 'LineWidth', alw, 'YTick',[0 0.2 0.4 0.6 0.8 1]); %<- Set properties
subplot(5,1,4);
plot(x_axis, X_shift(tmp_idx(sample_id(4)),:),'-*','Color',[0 0.447 0.741],'LineWidth',lw,'MarkerSize',msz);
title('Sample 4','FontSize',fsz);
grid on;
set(gca, 'fontsize', fsz, 'LineWidth', alw, 'YTick',[0 0.2 0.4 0.6 0.8 1]); %<- Set properties
subplot(5,1,5);
plot(x_axis, X_shift(tmp_idx(sample_id(5)),:),'-*','Color',[0 0.447 0.741],'LineWidth',lw,'MarkerSize',msz);
title('Sample 5','FontSize',fsz);
grid on;
set(gca, 'fontsize', fsz, 'LineWidth', alw, 'YTick',[0 0.2 0.4 0.6 0.8 1]); %<- Set properties
xlabel('Time of Eating Event','FontSize',fsz);
set(gcf, 'Position', [pos(1) pos(2) width*100, height*100]); %<- Set size

%% Visualization of K-means centroids
h = figure(2);
x_axis = 0:23;
width = 10;     % Width in inches
height = 5;    % Height in inches
alw = 1.5;    % AxesLineWidth
fsz = 32;      % Fontsize
lw = 5;      % LineWidth
msz = 18;       % MarkerSize
pos = get(gcf, 'Position');
p1 = plot(x_axis, centers(1,:),'-*','Color',[0 0.447 0.741],'LineWidth',lw,'MarkerSize',msz); hold on;
p2 = plot(x_axis, centers(2,:),'-o','Color',[0.85 0.325 0.098],'LineWidth',lw,'MarkerSize',msz); hold on;
p3 = plot(x_axis, centers(3,:),'-+','Color',[0.929 0.694 0.125],'LineWidth',lw,'MarkerSize',msz); hold on;
p4 = plot(x_axis, centers(4,:),'-x','Color',[0.494 0.184 0.556],'LineWidth',lw,'MarkerSize',msz); hold off;
% p4 = plot(x_axis, centers(5,:),'-+','Color',[0 1 0],'LineWidth',lw,'MarkerSize',msz); hold on;
% p6 = plot(x_axis, centers(6,:),'-+','Color',[0 0 0],'LineWidth',lw,'MarkerSize',msz); hold off;
% plot(x_axis, centers(7,:),'-+','Color',[0 0 0],'LineWidth',lw,'MarkerSize',msz); hold on;
% plot(x_axis, centers(8,:),'-+','Color',[0 0 0],'LineWidth',lw,'MarkerSize',msz); hold on;
% plot(x_axis, centers(9,:),'-+','Color',[0 0 0],'LineWidth',lw,'MarkerSize',msz); hold on;
% plot(x_axis, centers(5,:),'-+','Color',[0 0 0],'LineWidth',lw,'MarkerSize',msz); hold off;

legend([p1 p2 p3 p4], 'C1:256','C2:304','C3:405','C4:54');
xlim([0 24]); ylim([0 0.2]);
xlabel('Time of Eating Event','FontSize',fsz);
ylabel('Normalized daily distribution','FontSize',fsz);
grid on;
set(gcf, 'Position', [pos(1) pos(2) width*100, height*100]); %<- Set size
set(gca, 'fontsize', fsz, 'LineWidth', alw); %<- Set properties
print_to_pdf(h,'C:\Users\jason\Desktop\KDD paper\Hybrid results\centers_OT_k4.pdf');
