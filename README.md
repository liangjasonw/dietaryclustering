1. Matlab Main function:
"DietaryClusteringMain.m": Implement OT-DTW and all other clustering methods for comparison

2. Other functions:
"barycenter_compare.m": Compare outputs of Wasserstein barycenter and DTW barycenter.
"EROT_Wasserstein.m": Calculate entropic regularized Wasserstein distance.
"LOROT_Wasserstein.m": Calculate order-preserving Wasserstein distance.