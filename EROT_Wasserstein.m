function [Dist,T] = EROT_Wasserstein(a,centroids,ground_d,eps,tol)
% distance: entropy_Wasserstein
% Inputs:
% a: sample sequence with dim: 1 * len_1.
% centroids: K cluster centroids with dim: K * len_2.
% ground_d: ground distance matrix
% eps: parameter in front of entropy
% tol: threshold for early stop
    
    [~,len] = size(a);
	[K,~] = size(centroids);
    M = exp(-ground_d / eps);
    Dist = zeros(1,K);
    maxiter = 100; % maxiter
    T = zeros(len);
        
    for cluster_id = 1:K
        compt = 0;
        diff = inf;
        b = centroids(cluster_id,:)';
        % intialize u and v
        v = ones(len,1);
        while (compt < maxiter) && (diff > tol)
            compt = compt + 1;
            T_prev = T;
            % update u and v
            u = a' ./ (M*v);
            v = b ./ (M'*u);
            % assemble T 
            T = diag(v) * M * diag(u); % transport matrix
            % check diff between T and T_prev
            diff = norm(T - T_prev,'fro');
        end
        Dist(cluster_id) = sum(sum(ground_d.*T));
    end
    
end