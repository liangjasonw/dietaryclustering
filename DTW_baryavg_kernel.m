function ksc = DTW_baryavg_kernel(member_id, A, k)
%Computes centroid
    a = A(member_id==k,:);

    if size(a,1) == 0
        ksc = zeros(1, size(A,2)); 
        return;
    end

    ksc = DBA_kernel(a);

end


