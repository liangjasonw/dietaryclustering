% Trial of optimal transport using Sinkhorn-Knopp algorithm
% blog link:
% "https://regularize.wordpress.com/2015/09/17/calculating-transport-plans-with-sinkhorn-knopp/"

%% Examples in blog post
% % Parameters
% gamma = 10; % reg for entropy
% maxiter = 100; % maxiter
% map = colormap(gray);
%  
% N = 100; % size
% x = linspace(0,1,N)'; % spatial coordinate
% 
% % marginals
% p = exp(-(x-0.2).^2*10^2) + exp(-abs(x-0.4)*20);p=p./sum(p); %for colums sum
% q = exp(-(x-0.8).^2*10^2);q = q./sum(q); % for row sum
% [i,j] = meshgrid(1:N);
% M = exp(-(i-j).^2/gamma); % exp(-cost/gamma)
%  
% % intialize u and v
% u = ones(N,1); v = ones(N,1);
%  
% % Sinkhorn-Knopp iteratively scale rows and columns
% for k = 1:maxiter
%     % update u and v
%     u = p./(M*v);
%     v = q./(M'*u);
%     % assemble pi (only for illustration purposes)
%     pi = diag(v)*M*diag(u);
%     % display pi (with marginals on top and to the left)
%     imagesc([p'/max(p) 0;pi/max(pi(:)) q/max(q)])
%     colormap(1-map)
%     drawnow
%     pause(0.1);
% end

%% Test on real nutrition time series, EROT distance
% distribution readin
p = Y(4,:)'; % for colums sum
q = Y(5,:)'; % for row sum
% Parameters
len = size(p,1);
eps = 0.01; % reg for entropy
maxiter = 100; % maxiter
map = colormap(gray);


figure(1);
width = 6;     % Width in inches
height = 2.25;    % Height in inches
alw = 0.75;    % AxesLineWidth
fsz = 36;      % Fontsize
lw = 5;      % LineWidth
msz = 20;       % MarkerSize
pos = get(gcf, 'Position');
plot(p,'-o','LineWidth',lw,'MarkerSize',msz); 
xlim([0 25]); ylim([0 1]);
xlabel('Time of Eating Event for sample 1','FontSize',fsz);
ylabel('Normalized daily distribution','FontSize',fsz);
set(gcf, 'Position', [pos(1) pos(2) width*100, height*100]); %<- Set size
set(gca, 'fontsize', fsz, 'LineWidth', alw); %<- Set properties
grid on;

figure(2);
plot(q,'-o','LineWidth',lw,'MarkerSize',msz);
xlim([0 25]); ylim([0 1]);
xlabel('Time of Eating Event for sample 3','FontSize',fsz);
ylabel('Normalized daily distribution','FontSize',fsz);
set(gcf, 'Position', [pos(1) pos(2) width*100, height*100]); %<- Set size
set(gca, 'fontsize', fsz, 'LineWidth', alw); %<- Set properties
grid on;
% legend('p','q');

[i,j] = meshgrid(1:len);
ground_d = abs(i-j);
ground_d = ground_d / max(max(ground_d));
M = exp(-ground_d / eps); % exp(-cost/gamma)
% intialize v
v = ones(len,1);

figure(3);
% Sinkhorn-Knopp iteratively scale rows and columns
for k = 1:maxiter
    % update u and v
    u = p./(M*v);
    v = q./(M'*u);
    % assemble pi (only for illustration purposes)
    T = diag(v) * M * diag(u);
    % display pi (with p on top and q to the right)
    imagesc([p'/max(p) 0;T/max(T(:)) q/max(q)])
    colormap(1-map)
    drawnow
%     pause(0.2);
end
obj_EROT = sum(sum(ground_d.*T));

%% Test on real nutrition time series, LOROT distance
% distribution readin
p = Y(4,:)'; % for colums sum
q = Y(5,:)'; % for row sum
% Parameters
len = size(p,1);
% regularizer parameters
lambda_1 = 1;
lambda_2 = 1;
delta = 1; % parameter for weighted shift
sigma = 2; % parameter for Gaussian distribution G
maxiter = 100; % maxiter
map = colormap(gray);

figure(1);
width = 6;     % Width in inches
height = 2.25;    % Height in inches
alw = 0.75;    % AxesLineWidth
fsz = 36;      % Fontsize
lw = 5;      % LineWidth
msz = 20;       % MarkerSize
pos = get(gcf, 'Position');
plot(p,'-o','LineWidth',lw,'MarkerSize',msz); 
xlim([0 25]); ylim([0 1]);
xlabel('Time of Eating Event for sample 1','FontSize',fsz);
ylabel('Normalized daily distribution','FontSize',fsz);
set(gcf, 'Position', [pos(1) pos(2) width*100, height*100]); %<- Set size
set(gca, 'fontsize', fsz, 'LineWidth', alw); %<- Set properties
grid on;

figure(2);
plot(q,'-o','LineWidth',lw,'MarkerSize',msz);
xlim([0 25]); ylim([0 1]);
xlabel('Time of Eating Event for sample 3','FontSize',fsz);
ylabel('Normalized daily distribution','FontSize',fsz);
set(gcf, 'Position', [pos(1) pos(2) width*100, height*100]); %<- Set size
set(gca, 'fontsize', fsz, 'LineWidth', alw); %<- Set properties
grid on;
% legend('p','q');

[i,j] = meshgrid(1:len);
ground_d = abs(i-j); % ground distance
MAX_COST = (24-1)^2;
M = exp(-1/lambda_2 * (ground_d - lambda_1./(ground_d.^2/MAX_COST+delta))); % exp(-cost/gamma)
G = 1/(sigma*sqrt(2*pi)) * exp(-(i-j).^2/(2*sigma^2));
% intialize v
v = exp(-0.5/lambda_2) * ones(len,1);
K = G .* M; % the kernel projected on

figure(3);
% Sinkhorn-Knopp iteratively scale rows and columns
for k = 1:maxiter
    % update u and v
    u = p ./ (K*v);
    v = q ./ (K'*u);
    % assemble pi (only for illustration purposes)
    T = diag(u) * K * diag(v);
    % display pi (with p on top and q to the right)
    imagesc([q'/max(q) 0;T/max(T(:)) p/max(p)])
    colormap(1-map)
    drawnow
%     pause(0.2);
end
obj_LOROT = sum(sum(ground_d.*T));

%% comparison with cDTW
window = 24;
[Dist,~] = cDTW(p',q',window);
D_euc = ED(p',q');