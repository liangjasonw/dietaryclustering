%*******************************************************************************
 % Copyright (C) 2018 Liang Wang
 % 
 % This program is free software: you can redistribute it and/or modify
 % it under the terms of the GNU General Public License as published by
 % the Free Software Foundation, version 3 of the License.
 % 
 % This program is distributed in the hope that it will be useful,
 % but WITHOUT ANY WARRANTY; without even the implied warranty of
 % MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 % GNU General Public License for more details.well
 % 
 % You should have received a copy of the GNU General Public License
 % along with this program.  If not, see <http://www.gnu.org/licenses/>.
 %*****************************************************************************

% Dietary Temporal Pattern Clustering project
% This main code implements our two-stage OT-DTW clustering algorithm

clc; clear all;
%% Data Readin
data_dir = 'C:\Users\jason\Box Sync\Research\Public health\Collarboration with Yikyung\Data\';
dat = load(fullfile(data_dir,'energy_hourly_1.mat')); % dataset saves in variable "X"
X = dat.x;
dat = load(fullfile(data_dir,'energy_hourly_all.mat')); % dataset saves in variable "Y"
fat_h = load(fullfile(data_dir,'fat_hourly_all.mat')); % dataset saves in variable "x"
carb_h = load(fullfile(data_dir,'carb_hourly_all.mat')); % dataset saves in variable "x"
prot_h = load(fullfile(data_dir,'prot_hourly_all.mat')); % dataset saves in variable "x"
Y_fat = fat_h.x'; Y_carb = carb_h.x'; Y_prot = prot_h.x';
Y = dat.x';
Y_all = Y_fat + Y_carb + Y_prot;
Y_all_ratio = bsxfun(@rdivide, Y_all, sum(Y_all,2)); % calculated from sum of fat, carb and protein, different than Y
[num_sample, ts_len] = size(X);
K_outter = 5; % number of clusters
% shift original data to avoid 0's
delta = 1e-6;
X_tmp = X + delta;
X_shift = bsxfun(@times, X_tmp, 1./(sum(X_tmp, 2)));

%% Two-stage OT-DTW algorithm

%% 1st stage Entropy regularized OT-means
tic;
run_num = 1000; % number of randomized initializations
init_id_EROT = zeros(num_sample,run_num);
member_id_EROT = zeros(num_sample,run_num);
centers_EROT = zeros(K_outter,ts_len,run_num);
obj_EROT = zeros(run_num,1);
parfor i = 1:run_num
    disp(i);
    [init_id, member_id, centers, obj_opt, ~] = kMeans_EROT(X_shift, K_outter);
    init_id_EROT(:,i) = init_id;
    member_id_EROT(:,i) = member_id;
    centers_EROT(:,:,i) = centers;
    obj_EROT(i) = obj_opt;
end
toc;
% % post-processing of clustering results
% % load('EROT_results.mat');
[sorted_obj_EROT,I] = sort(obj_EROT);
h = figure(1);
plot(sorted_obj_EROT,'LineWidth',lw);
xlabel('run id','FontSize',fsz);
ylabel('objective value','FontSize',fsz);
set(gca, 'fontsize', fsz, 'LineWidth', alw); %<- Set properties
% print_to_pdf(h,'C:\Users\jason\Desktop\KDD paper\Hybrid results\converg_k5.pdf');
% title('1000 runs with randomized initializations for OT-Means at K=5')
best_init_id_EROT = init_id_EROT(:, I(5));
[init_id, member_id, centers, obj_opt, clusters] = kMeans_EROT(X_shift, K_outter, best_init_id_EROT);
% save('EROT_results_k5_1000.mat','init_id_EROT','member_id_EROT','centers_EROT','obj_EROT');

%% 2nd stage DTW-means
tmp_idx = find(member_id==1);
dat_to_cluster = X_shift(tmp_idx,:);
K_inner = 4;
tic;
run_num = 100; % number of randomized initializations
[num_sample_inner,~] = size(dat_to_cluster);
init_id_DTW = zeros(num_sample_inner,run_num);
member_id_DTW = zeros(num_sample_inner,run_num);
centers_DTW = zeros(K_inner,ts_len,run_num);
obj_DTW = zeros(run_num,1);
parfor i = 1:run_num
    disp(i);
    [init_id_new, member_id_new, centers_new, obj_opt_new, ~] = kMeans_DTW(dat_to_cluster, K_inner);
    init_id_DTW(:,i) = init_id_new;
    member_id_DTW(:,i) = member_id_new;
    centers_DTW(:,:,i) = centers_new;
    obj_DTW(i) = obj_opt_new;
end
toc;
% load('DTW_subcluster_5_k4_100.mat');
[sorted_obj_DTW,I] = sort(obj_DTW);
plot(sorted_obj_DTW,'LineWidth',lw);
best_init_id_DTW = init_id_DTW(:, I(1));
[init_id_new, member_id_new, centers_new, obj_opt_new, clusters_new] = kMeans_DTW(dat_to_cluster, K_inner, best_init_id_DTW);
% save('DTW_subcluster_5_k4_100.mat','init_id_DTW','member_id_DTW','centers_DTW','obj_DTW');

%% Locally regularized OT + K-means
% tic;
% [member_id, centers, clusters] = kMeans_LOROT(Y, K);
% toc;

%% K-medoids clustering
% tic;
% DistanceIndex = 3;    % ED=1,NCCc=2,cDTW=3, LOROT_Wasserstein=4
% DM = DMComputation(X, DistanceIndex);
% medoids = randsample(num_sample, K); % randomly pick K existing data samples as medoids.
% [member_id, new_medoids, cost, clusters] = PartitioningAroundMedoids(medoids,DM); % PAM clustering
% toc;

%% other clustering methods
% DTW_Window = length; % max value = time series length
% tic;
% [member_id, centers, clusters] = kMeans_DTW(x, K, DTW_Window);
% % [member_id, centers, clusters] = kDBA(x, K, DTW_Window);
% % [member_id, centers, clusters] = kShape(x, K);
% % [member_id, centers, clusters] = KSC(x, K);
% toc;
% save('C:\Users\jason\Box Sync\Research\Public health\Collarboration with Yikyung\Data\clusters_info_PAM_DTW.mat','member_id','new_medoids','clusters');

%% Fuzzy c-means
% [Member_Deg, centers] = FuzzyCmeans(x,K);

%% Visualization of 2nd stage DTW-means centroids
h = figure(2);
x_axis = 0:23;
width = 12.5;     % Width in inches
height = 5;    % Height in inches
alw = 1.5;    % AxesLineWidth
fsz = 48;      % Fontsize
lw = 5;      % LineWidth
msz = 18;       % MarkerSize
pos = get(gcf, 'Position');
p1 = plot(x_axis, centers_new(1,:),'-*','Color',[0 0.447 0.741],'LineWidth',lw,'MarkerSize',msz); hold on;
p2 = plot(x_axis, centers_new(2,:),'-o','Color',[0.85 0.325 0.098],'LineWidth',lw,'MarkerSize',msz); hold on;
p3 = plot(x_axis, centers_new(3,:),'-+','Color',[0.929 0.694 0.125],'LineWidth',lw,'MarkerSize',msz); hold on;
p4 = plot(x_axis, centers_new(4,:),'-x','Color',[0.494 0.184 0.556],'LineWidth',lw,'MarkerSize',msz); hold off;
% p1 = plot(x_axis, centers(1,:),'-*','Color',[0 0.447 0.741],'LineWidth',lw,'MarkerSize',msz); hold on;
% p2 = plot(x_axis, centers(2,:),'-o','Color',[0.85 0.325 0.098],'LineWidth',lw,'MarkerSize',msz); hold on;
% p3 = plot(x_axis, centers(3,:),'-+','Color',[0.929 0.694 0.125],'LineWidth',lw,'MarkerSize',msz); hold on;
% p4 = plot(x_axis, centers(4,:),'-x','Color',[0.494 0.184 0.556],'LineWidth',lw,'MarkerSize',msz); hold on;
% p5 = plot(x_axis, centers(5,:),'-+','Color',[0 0 0],'LineWidth',lw,'MarkerSize',msz); hold off;
% legend([p1 p2 p3 p4], 'C1:89','C2:54','C3:147','C4:102');
legend([p1 p2 p3 p4], 'C1:132','C2:32','C3:17','C4:72');
xlim([0 24]); ylim([0 1]);
xlabel('Time of Eating Event (hour)','FontSize',fsz);
ylabel('Normalized Ratio','FontSize',fsz);
grid on;
set(gcf, 'Position', [pos(1) pos(2) width*100, height*100]); %<- Set size
set(gca, 'fontsize', fsz, 'LineWidth', alw); %<- Set properties
print_to_pdf(h,'C:\Users\jason\Desktop\KDD paper\Hybrid results\centers_DTW_5_k4.pdf');

%% Visualization of some random samples
figure(1);
x_axis = 0:23;
width = 10;     % Width in inches
height = 75;    % Height in inches
alw = 1.5;    % AxesLineWidth
fsz = 24;      % Fontsize
lw = 3;      % LineWidth
msz = 12;       % MarkerSize
pos = get(gcf, 'Position');
sample_id = find(member_id_new==2);

subplot(5,1,1);
plot(x_axis, X_shift(tmp_idx(sample_id(6)),:),'-*','Color',[0 0.447 0.741],'LineWidth',lw,'MarkerSize',msz);
title('Sample 1','FontSize',fsz);
grid on;
set(gca, 'fontsize', fsz, 'LineWidth', alw); %<- Set properties
subplot(5,1,2);
plot(x_axis, X_shift(tmp_idx(sample_id(7)),:),'-*','Color',[0 0.447 0.741],'LineWidth',lw,'MarkerSize',msz);
title('Sample 2','FontSize',fsz);
grid on;
set(gca, 'fontsize', fsz, 'LineWidth', alw); %<- Set properties
subplot(5,1,3);
plot(x_axis, X_shift(tmp_idx(sample_id(8)),:),'-*','Color',[0 0.447 0.741],'LineWidth',lw,'MarkerSize',msz);
title('Sample 3','FontSize',fsz);
grid on;
set(gca, 'fontsize', fsz, 'LineWidth', alw); %<- Set properties
subplot(5,1,4);
plot(x_axis, X_shift(tmp_idx(sample_id(9)),:),'-*','Color',[0 0.447 0.741],'LineWidth',lw,'MarkerSize',msz);
title('Sample 4','FontSize',fsz);
grid on;
set(gca, 'fontsize', fsz, 'LineWidth', alw); %<- Set properties
subplot(5,1,5);
plot(x_axis, X_shift(tmp_idx(sample_id(12)),:),'-*','Color',[0 0.447 0.741],'LineWidth',lw,'MarkerSize',msz);
xlabel('Time of Eating Event','FontSize',fsz);
title('Sample 5','FontSize',fsz);
grid on;
set(gca, 'fontsize', fsz, 'LineWidth', alw); %<- Set properties
set(gcf, 'Position', [pos(1) pos(2) width*100, height*100]); %<- Set size

%% Visualization of 1st stage OT-means centroids
h = figure(1);
x_axis = 0:23;
width = 10;     % Width in inches
height = 5;    % Height in inches
alw = 1.5;    % AxesLineWidth
fsz = 32;      % Fontsize
lw = 5;      % LineWidth
msz = 18;       % MarkerSize
pos = get(gcf, 'Position');

p1 = plot(x_axis, centers(5,:),'-*','Color',[0 0.447 0.741],'LineWidth',lw,'MarkerSize',msz); hold on;
% p2 = plot(x_axis, centers(2,:),'-o','Color',[0.85 0.325 0.098],'LineWidth',lw,'MarkerSize',msz); hold on;
% p3 = plot(x_axis, centers(3,:),'-+','Color',[0.929 0.694 0.125],'LineWidth',lw,'MarkerSize',msz); hold on;
% p4 = plot(x_axis, centers(4,:),'-x','Color',[0.494 0.184 0.556],'LineWidth',lw,'MarkerSize',msz); hold on;
% p5 = plot(x_axis, centers(5,:),'-+','Color',[0 0 0],'LineWidth',lw,'MarkerSize',msz); hold off;
% legend([p1 p2 p3 p4], 'C1:81','C2:70','C3:41','C4:78');
xlim([0 24]); ylim([0 0.2]);
title('Cluster centroid 5','FontSize',fsz);
xlabel('Time of Eating Event (hour)','FontSize',fsz);
ylabel('Normalized Ratio','FontSize',fsz);
grid on;
set(gcf, 'Position', [pos(1) pos(2) width*100, height*100]); %<- Set size
set(gca, 'fontsize', fsz, 'LineWidth', alw); %<- Set properties
% print_to_pdf(h,'C:\Users\jason\Desktop\KDD paper\Hybrid results\centers_DTW_2_k4.pdf');

%% Visualization of nutrient compositions
h = figure(1);
x_axis = 0:23;
width = 10;     % Width in inches
height = 5;    % Height in inches
alw = 1.5;    % AxesLineWidth
fsz = 32;      % Fontsize
lw = 5;      % LineWidth
msz = 18;       % MarkerSize
pos = get(gcf, 'Position');
p1 = plot(x_axis, Y_all_ratio(1,:),'-*','Color',[0 0.447 0.741],'LineWidth',lw,'MarkerSize',msz); hold on;
% p2 = plot(x_axis, Y_carb(8,:),'--','Color',[0.85 0.325 0.098],'LineWidth',lw,'MarkerSize',msz); hold on;
% p3 = plot(x_axis, Y_fat(8,:),'--','Color',[0.929 0.694 0.125],'LineWidth',lw,'MarkerSize',msz); hold on;
% p4 = plot(x_axis, Y_prot(8,:),'--','Color',[0.494 0.184 0.556],'LineWidth',lw,'MarkerSize',msz); hold off;
xlabel('Time of Eating Event (hour)','FontSize',fsz);
ylabel('Normalized Ratio','FontSize',fsz);
% legend([p1 p2 p3 p4], 'Total intake', 'Carbohydrate intake', 'Fat intake', 'Protein intake');
grid on;
set(gca, 'fontsize', fsz, 'LineWidth', alw); %<- Set properties
set(gcf, 'Position', [pos(1) pos(2) width*100, height*100]); %<- Set size
% print_to_pdf(h,'C:\Users\jason\Desktop\KDD paper\Hybrid results\intro_sample2.pdf');

%% plot 1st largest eating event of each cluster
% [~,I] = max(x,[],2);
% max_of_cluster = zeros(4,24);
% for time_id = 1:24
%     for clus_id = 1:4
%         chosen_id = find(member_id == clus_id);
%         max_of_cluster(clus_id,time_id) = sum(I(chosen_id) == time_id) / clusters(clus_id);
%     end
% end
% figure(2);
% x_axis = 0:23;
% width = 6;     % Width in inches
% height = 4.5;    % Height in inches
% alw = 0.75;    % AxesLineWidth
% fsz = 36;      % Fontsize
% lw = 5;      % LineWidth
% msz = 20;       % MarkerSize
% pos = get(gcf, 'Position');
% plot(x_axis, max_of_cluster(1,:), '-*','LineWidth',lw,'MarkerSize',msz); hold on;
% plot(x_axis, max_of_cluster(2,:), '-o','LineWidth',lw,'MarkerSize',msz); hold on;
% plot(x_axis, max_of_cluster(3,:), '-+','LineWidth',lw,'MarkerSize',msz); hold on;
% plot(x_axis, max_of_cluster(4,:), '-x','LineWidth',lw,'MarkerSize',msz); hold on;
% hold off;
% legend('C1','C2','C3','C4');
% xlim([0 23]); ylim([0 0.35]);
% xlabel('Time of the largest Eating Event (in Hours)','FontSize',fsz);
% ylabel('Probability','FontSize',fsz);
% set(gcf, 'Position', [pos(1) pos(2) width*100, height*100]); %<- Set size
% set(gca, 'fontsize', fsz, 'LineWidth', alw); %<- Set properties

%% plot 2nd largest eating event of each cluster
% [~,I_all] = sort(x,2);
% I = I_all(:,23);
% max2_of_cluster = zeros(4,24);
% for time_id = 1:24
%     for clus_id = 1:4
%         chosen_id = find(member_id == clus_id);
%         max2_of_cluster(clus_id,time_id) = sum(I(chosen_id) == time_id) / clusters(clus_id);
%     end
% end
% figure(3);
% x_axis = 0:23;
% width = 6;     % Width in inches
% height = 4.5;    % Height in inches
% alw = 0.75;    % AxesLineWidth
% fsz = 36;      % Fontsize
% lw = 5;      % LineWidth
% msz = 20;       % MarkerSize
% pos = get(gcf, 'Position');
% plot(x_axis, max2_of_cluster(1,:), '-*','LineWidth',lw,'MarkerSize',msz); hold on;
% plot(x_axis, max2_of_cluster(2,:), '-o','LineWidth',lw,'MarkerSize',msz); hold on;
% plot(x_axis, max2_of_cluster(3,:), '-+','LineWidth',lw,'MarkerSize',msz); hold on;
% plot(x_axis, max2_of_cluster(4,:), '-x','LineWidth',lw,'MarkerSize',msz); hold on;
% hold off;
% legend('C1','C2','C3','C4');
% xlim([0 23]); ylim([0 0.35]);
% xlabel('Time of the 2nd largest Eating Event (in Hours)','FontSize',fsz);
% ylabel('Probability','FontSize',fsz);
% set(gcf, 'Position', [pos(1) pos(2) width*100, height*100]); %<- Set size
% set(gca, 'fontsize', fsz, 'LineWidth', alw); %<- Set properties
