function [init_id, member_id, centers, obj_opt, clusters] = kMeans_EROT(A, K, best_init_id)
% distance: 2-Wasserstein distance
% barycenter averaging: Wasserstein barycenter
% Inputs:
% A: input time series samples
% K: pre-defined number of clusters
% best_init_id: initialized cluster membership ids
% Outputs:
% init_id: current initialized cluster membership ids
% member_id: clustering output cluster membership ids
% centers: cluster centroids, dim: K * len
% obj_opt: sum of variance of all clusters 
% clusters: saves the size of each cluster

    [m,len] = size(A); % num-of-samples * sample_length
    if nargin < 3
        member_id = ceil(K * rand(m, 1)); % save cluster id of each sample
    else
        member_id = best_init_id;
    end
    init_id = member_id;
    centers = zeros(K,len); % save centers of each cluster
    D = zeros(m,K); % sample-to-center distance matrix
    % ground distance matrix
    [i,j] = meshgrid(1:len);
    ground_d = (i-j).^2; % (i-j).^2 or abs(i-j)
    ground_d = ground_d / max(ground_d(:));
    % barycenter parameters
    Max_iter = 100;
    eps = 1e-2;
    useGPU = false;
    tol = 1e-4;
    obj = inf;

    for iter = 1:Max_iter
%         disp(iter);
        prev_mem = member_id;
        prev_obj = obj;

        for k = 1:K
%             centers(k,:) = meanavg(member_id, A, k);
            tmp = entropy_baryavg(A(member_id==k,:),ground_d,eps,useGPU,tol);
            centers(k,:) = tmp';
        end

        for i = 1:m
            [D(i,:),~] = EROT_Wasserstein(A(i,:),centers,ground_d,eps,tol);
        end

        [dist_to_center, member_id] = min(D,[],2);
        obj = sumsqr(dist_to_center);
%         disp(obj);
        if norm(prev_mem - member_id) == 0 || (prev_obj - obj) < 1e-5
            break;
        end
    end
    % save the optimal objective
    obj_opt = sumsqr(dist_to_center);
    
    % save how many samples in each cluster
    clusters = zeros(1,K);
    for k = 1:K
        clusters(k) = length(find(member_id == k));
    end

end
