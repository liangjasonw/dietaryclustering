%%% Compare DBA and Wasserstein Barycenter
% test of DBA
% sequences = rand(100,20);
% mean = DBA(sequences);
% plot(mean);

%% self-made examples
a1 = zeros(1,24); a1(7) = 0.2; a1(12) = 0.3; a1(18) = 0.5;
a2 = zeros(1,24); a2(9) = 0; a2(14) = 0.3; a2(20) = 0.7;
A = [a1;a2];
[~,len] = size(A);
% A = X_shift(3:4,:);
cur_center = zeros(1,24);
DBA_avg = DBA(A);
[i,j] = meshgrid(1:len);
ground_d = (i-j).^2; % (i-j).^2 or abs(i-j)
ground_d = ground_d / max(ground_d(:));
eps = 1e-2;
useGPU = false;
tol = 1e-4;
EROT_avg = entropy_baryavg(A,ground_d,eps,useGPU,tol);

%% plot
alw = 1.5;    % AxesLineWidth
fsz = 24;      % Fontsize
lw = 3;      % LineWidth

h = figure(1);
plot(A(1,:),'LineWidth',lw); hold on;
plot(A(2,:),'LineWidth',lw); grid on; hold off;
xlabel('Time of Eating Event (hour)','FontSize',fsz);
ylabel('Normalized Ratio','FontSize',fsz);
set(gca, 'fontsize', fsz, 'LineWidth', alw); %<- Set properties
% print_to_pdf(h,'C:\Users\jason\Desktop\KDD paper\Hybrid results\barycenter_1.pdf');

h = figure(2);
plot(DBA_avg,'Color',[0 0.447 0.741],'LineWidth',lw); grid on;
xlabel('Time of Eating Event (hour)','FontSize',fsz);
ylabel('Normalized Ratio','FontSize',fsz);
set(gca, 'fontsize', fsz, 'LineWidth', alw); %<- Set properties
% print_to_pdf(h,'C:\Users\jason\Desktop\KDD paper\Hybrid results\barycenter_2.pdf');

h = figure(3);
plot(EROT_avg,'Color',[0 0.447 0.741],'LineWidth',lw); grid on;
xlabel('Time of Eating Event (hour)','FontSize',fsz);
ylabel('Normalized Ratio','FontSize',fsz);
set(gca, 'fontsize', fsz, 'LineWidth', alw); %<- Set properties
% print_to_pdf(h,'C:\Users\jason\Desktop\KDD paper\Hybrid results\barycenter_3.pdf');