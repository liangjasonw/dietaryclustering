function print_to_pdf(h, filename)
% This function is to print matlab figure to pdf for inserting to Latex.
% Inputs:
% h: handle for the figure.
% filename: output filepath.

    set(h,'Units','Inches');
    pos = get(h,'Position');
    set(h,'PaperPositionMode','Auto','PaperUnits','Inches','PaperSize',[pos(3), pos(4)])
    print(h,filename,'-dpdf','-r0')
end